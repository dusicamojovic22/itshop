<ul>
    <li><a class="active" href="#shipping.html">Shipping</a></li>
    <li><a href="#faq">FAQ</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#trackorder">Track Order</a></li>
  </ul>

  <ul class="nav">
    <li>
      <p>TECHNOLOGY</p>
    </li>
    <li>
      <p>Send us a message</p>
      <p>demo@demo.com</p>
    </li>
    <li>
      <p>Need help? Call Us:</p>
      <p><b>012 345 6789</b></p>
    </li>
    <li class="icon">
      <i class="fa fa-user"></i>
    </li>
  </ul>