<?php
require_once 'AutoClass.php';

class AutoController
{
    function __construct()
    {
        $this->automobili = [
            new Auto("Skoda",3000, 2005, '2022-05-06'),
            new Auto("BMW",4000, 2005, '2013-05-06'),
            new Auto("Skoda",5000, 2008, '2022-05-06'),
            new Auto("Toyota",6000, 2006, '2017-05-09')
        ];
    }
    
    public function prikazPoGodistu()
    {
        $godiste = isset($_GET['godiste']) ? $_GET['godiste'] : '';
       // die($godiste);
        if ($godiste == '') {
            $errGodiste = 'Morate uneti godiste';
            include_once 'index.php';
        } else {
            $automobili2 = [];

            foreach ($this->automobili as $pom)
                if ($pom->godiste == $godiste)
                    $automobili2[] = $pom;

            $automobili = $automobili2;
            include_once 'listaAuta.php';
        }
    }

    public function prikazPoDanu()
    {
        $danKupovine = isset($_GET['danKupovine']) ? $_GET['danKupovine'] : '';
        //var_dump($dan);
        
        if ($danKupovine == '') {
            $errDan = 'Morate uneti dan';
            include_once 'index.php';
        } else {
            $auto = NULL;
            echo 'test';
            foreach ($this->automobili as $pom){
                //var_dump($pom->danKupovine);
                if ($pom->danKupovine == $danKupovine){
                    $auto = $pom;
                    break;
                }
            }
            //var_dump($auto);
            //die();
            include_once 'auto.php';
        }
    }

    public function unos(){

    }

    public function prikazSvih(){
        $automobili = $this->automobili;
        include_once 'listaAuta.php';
    }
}
