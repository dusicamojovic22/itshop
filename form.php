
  <div class="container mt-3">
  <h2><b>Drop As a Line</b></h2>
  <p>Have a question or comment? Use the form below to send us a message or contact us by mail.</p>
  <form>
    <div class="row">
      <div class="col">       
        <select class="form-select">
          <option>Customer service</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
        </select>
      </div>
      <div class="col">
        <input type="text" class="form-control" placeholder="your@gmail.com" name="pswd">
      </div>
    </div>
  </form>
</div>
<div class="container mt-3">
  <div class="custom-file mb-3">
    <input type="file" class="custom-file-input" id="customFile" name="filename">
    <label class="custom-file-label" for="customFile">Choose file</label>
  </div>
  <script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    </script>
    <div class="container mt-3">
      <form action="/action_page.php">
        <div class="mb-3 mt-3">
          <textarea class="form-control" rows="5" id="comment" name="text"></textarea>
        </div>
        <button type="submit" class="p-2 btn btn-danger">Post Comment</button>
      </form>
    </div>