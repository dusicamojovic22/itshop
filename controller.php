<?php
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";

require_once 'AutoController.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // get akcije
    switch ($action) {
        case 'prikaz_svih':
            // kod akcije
            //echo 'GET prikaz_svih';
            //include_once 'listaAuta.php';

            $autoController = new AutoController();
            $autoController->prikazSvih();

            break;
        case 'prikaz_po_godistu':
            // kod akcije
            //echo 'GET prikaz_po_godistu';
            //$godiste = 2008;
           
            $autoController = new AutoController();
            $autoController->prikazPoGodistu();
            break;
        case 'prikaz_po_danu':
            //echo 'GET prikaz_po_danu';
            //$auto = $automobili[2];
            $autoController = new AutoController();
            $autoController->prikazPoDanu();
            break;
        default:
            echo 'ERROR WRONG GET ACTION';
            break;
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // post akcije
    switch ($action) {
        case 'Unos':
            // echo 'POST unos';
            //$auto = $automobili[1];
            //include_once 'auto.php';

            $autoController = new AutoController();
            $autoController->unos();

            break;
        default:
            echo 'ERROR WRONG POST ACTION';
            break;
    }
} else {
    echo 'ERROR METHOD';
}
